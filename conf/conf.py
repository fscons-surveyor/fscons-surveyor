# Coordinates are y, x! (row, col)
tickboxes = {
    'topleft':      (0.27, 0.39),
    'bottomright':  (0.51, 0.88),
    'collumns': 5,
    'rows': 12
    }

triangle1 = {
    'topleft':      (0.68, 0.12),
    'bottomright':  (0.83, 0.37)
    }

triangle2 = {
    'topleft':      (0.81, 0.34),
    'bottomright':  (0.97, 0.77)
    }

debug = True

conf = {
    'tickboxes': tickboxes,
    'triangle1': triangle1,
    'triangle2': triangle2,
    'debug': debug
    }
