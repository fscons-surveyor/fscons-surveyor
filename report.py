import numpy as np
import scipy.ndimage as ndi
import matplotlib.pyplot as plt
from rsrcs import triangles as tri


def read_data(filename='triangle1'):
    data = np.loadtxt('output/{}.out'.format(filename))

    return data


def get_renorm_points(data):
    xhat = np.array([data[:, -2] - data[:, -4], data[:, -1] - data[:, -3]])
    yhat = np.array([data[:, -6] - data[:, -4], data[:, -5] - data[:, -3]])
    xnorm = np.sqrt((xhat ** 2).sum(0))
    ynorm = np.sqrt((yhat ** 2).sum(0))

    ps = np.array([data[:, -11] - data[:, -4], data[:, -10] - data[:, -3]])
    x = (ps * xhat).sum(0) / (xnorm ** 2)
    y = (ps * yhat).sum(0) / (ynorm ** 2)

    print (xhat / xnorm).mean(1)
    print (yhat / ynorm).mean(1)

    return x, -np.sqrt(1 / 3.) * x + 2 / np.sqrt(3) * y


def get_heatmap(data, dim=1, size=100, nstd=2):
    heatmap = np.ones((np.round(size * np.sqrt(3) / 2).astype(np.int), size))
    x, y = get_renorm_points(data)

    ns = data[:, 12]
    if dim == 1:
        sigmas = data[:, 4] * size
    elif dim == 2:
        sigmas = np.sqrt(data[:, 8]) * size
    elif dim == 0:
        sigmas = np.ones(ns.shape) * np.sqrt(nstd * np.sqrt(3) * size ** 2 / \
                 (4 * np.pi * ns.size))

    for n, s in enumerate(sigmas):
        temp = np.zeros(heatmap.shape)
        print x[n] * size, y[n] * size, s
        temp[np.round(size * y[n]).astype(np.int),
             np.round(size * x[n]).astype(np.int)] = 255
        heatmap += ndi.gaussian_filter(temp, s, mode='constant') / 255.0

    corners = np.array([[0, 0], [0.5, np.sqrt(3) / 2], [1.0, 0]]) * size

    masked, mask = tri.wash_triangle(heatmap, corners, 0.0)

    return masked


def make_heatmap(data, dim=0, size=256, scatter=True, nstd=2):
    cs = np.array([[0, 0], [0.5, np.sqrt(3) / 2], [1.0, 0]]) * size
    centre = cs.mean(0)

    hm = get_heatmap(data, dim, size, nstd)
    hmean = hm[np.where(hm > 0)].mean()
    hmax  = hm[np.where(hm > 0)].max()
    hm = np.where(hm > 0, hm, np.nan)

    plt.imshow(hm, origin='lower')
    plt.plot(np.hstack((cs[:, 0], cs[0, 0])), np.hstack((cs[:, 1], cs[0, 1])),
             '1-k', lw=4, mew=3, ms=15)
    plt.plot([centre[0], cs[0, 0]], [centre[1], cs[0, 1]], 'w:')
    plt.plot([centre[0], cs[1, 0]], [centre[1], cs[1, 1]], 'w:')
    plt.plot([centre[0], cs[2, 0]], [centre[1], cs[2, 1]], 'w:')

    if scatter:
        x, y = get_renorm_points(data)
        plt.scatter(x * size, y * size)
        plt.plot(x.mean() * size, y.mean() * size,
                 'o', ms=10, mew=2, mec='k', mfc='none')
        plt.contour(hm, (0, hmean), linestyles='dashed')
        #plt.contour(hm, (0, hmean, 1 + (hmax - 1) * 0.5), linestyles='dashed')
        #plt.plot(x.mean() * size, y.mean() * size,
        #         '1', ms=10, mew=2, mec='k', mfc='none')

    plt.axis('image')
    plt.axis('off')
    plt.axis('equal')
    plt.text(size / 2, np.sqrt(3) / 2 * size * 1.02,
             'technology:\n{0:.2f}%'.format(data[:, 5].mean() * 100),
             ha='center', size=14)
    plt.text(-0.02 * size, 0,
             'society:\n{0:.2f}%'.format(data[:, 6].mean() * 100),
             ha='right', va='top', size=14)
    plt.text(size * 1.02, 0,
             'culture:\n{0:.2f}%'.format(data[:, 7].mean() * 100),
             ha='left', va='top', size=14)


def make_raw_scatter(data):
    xoffs = data[:, (-2, -4, -6)].mean(1)
    yoffs = data[:, (-1, -3, -5)].mean(1)

    cs = np.array([[0, 0], [0.5, np.sqrt(3) / 2], [1.0, 0]]) * 200
    centre = cs.mean(0)
    cs -= centre
    centre -= centre

    plt.scatter(data[:, (-2, -4, -6)] - np.array([xoffs]).T,
               -data[:, (-1, -3, -5)] + np.array([yoffs]).T)
    plt.scatter(data[:, -11] - xoffs, -data[:, -10] + yoffs)

    plt.axis('equal')
    plt.axis('off')

    plt.plot([centre[0], cs[0, 0]], [centre[1], cs[0, 1]], 'w:')
    plt.plot([centre[0], cs[1, 0]], [centre[1], cs[1, 1]], 'w:')
    plt.plot([centre[0], cs[2, 0]], [centre[1], cs[2, 1]], 'w:')


def make_report(fign=1, clearit=True, nstd=2):
    d1 = read_data('triangle1')
    d2 = read_data('triangle2')

    f = plt.figure(fign)
    if clearit:
        f.clf()

    plt.subplot(121)
    plt.title('2012', size='xx-large')
    make_heatmap(d1, nstd=nstd)

    plt.subplot(122)
    plt.title('2013', size='xx-large')
    make_heatmap(d2, nstd=nstd)
