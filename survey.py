# Initial imports:

import numpy as np
import matplotlib.pyplot as plt
from conf import Conf
from rsrcs import triangles as tri
from rsrcs import hough as ht


## Backbones:
def make_evaluation(which_survey='triangle1'):
    if which_survey.lower() == 'triangle1':
        numbers = np.loadtxt('data/triangle1.in').astype(np.int)
        data = np.zeros((numbers.size, 20))
        for n, number in enumerate(numbers):
            print "Evaluating survey {0}".format(str.zfill(repr(number), 4))
            data[n] = evaluate_triangle1(number)

        np.savetxt('output/triangle1.out', data)
        print "Wrote '{0}'.".format('output/triangle1.out')
    elif which_survey.lower() == 'triangle2':
        numbers = np.loadtxt('data/triangle2.in').astype(np.int)
        data = np.zeros((numbers.size, 20))
        for n, number in enumerate(numbers):
            print "Evaluating survey {0}".format(str.zfill(repr(number), 4))
            data[n] = evaluate_triangle2(number)

        np.savetxt('output/triangle2.out', data)
        print "Wrote '{0}'.".format('output/triangle2.out')
    elif which_survey.lower() == 'tickboxes':
        print "Tickboxes are not implemented!"
    else:
        print "Invalid option: {0}".format(which_survey)


def get_survey(number, prewash=False):
    image = plt.imread('data/surveys-{0}.png'.\
                       format(str.zfill(repr(number), 4)))
    image = flatten(image)
    dims = image.shape

    if prewash:
        template = plt.imread('templates/surveys-template.png')
        template = flatten(template)
        image = np.where(template > 0, image, 1)
        print image.max(), image.min()

    return image, dims


def flatten(image, cutoff=0.5):
    while np.rank(image) > 2:
        image = image.mean(-1)

    return np.round(image / image.max())


## Tickboxes: (Not implemented!)
def evaluate_tickbox(number):
    im, dims = get_survey(number)

    tl = np.round(np.array(Conf['tickboxes']['topleft']) * dims).\
         astype(np.int)
    br = np.round(np.array(Conf['tickboxes']['bottomright']) * dims).\
         astype(np.int)

    T = im[tl[0]: br[0], tl[1]: br[1]].copy()
    T -= T.max()
    T = np.abs(T)


## Triangle 1:
def evaluate_triangle1(number):
    im, dims = get_survey(number)

    tl = np.round(np.array(Conf['triangle1']['topleft']) * dims).\
         astype(np.int)
    br = np.round(np.array(Conf['triangle1']['bottomright']) * dims).\
         astype(np.int)

    T = im[tl[0]: br[0], tl[1]: br[1]].copy()
    T -= T.max()
    T = np.round(np.abs(T)).astype(bool)

    ### Compuations:
    r, t, H = ht.hough_transform(T, 0.2, 1.0)
    tmp_rhos, tmp_thetas = tri.get_maxima(H, neigh=8)  # 8 is a magic number
    rhos = r[np.round(tmp_rhos).astype(np.int)]
    thetas = t[np.round(tmp_thetas).astype(np.int)]
    ks, ms = tri.get_lines_params(rhos, thetas)
    lines = np.array([ks, ms]).T
    cs = np.array(tri.get_corners(lines))
    wT, mask = tri.wash_triangle(T, cs, 8)
    score1, delta1, p, sigma_p, pn = tri.get_score(wT, cs, 1)
    score2, delta2, p, sigma_p, pn = tri.get_score(wT, cs, 2)

    if Conf['debug']:
        print "Writing debug..."

        f1 = plt.figure(1)
        f1.clf()
        plt.title('survey-{0}'.format(str.zfill(repr(number), 4)))
        plt.subplot(121)
        plt.imshow(T, interpolation='none', cmap=plt.cm.gray_r)
        plt.plot(np.hstack((cs[:, 0], cs[0, 0])),
                 np.hstack((cs[:, 1], cs[0, 1])), 'r--1')
        plt.plot(p[0], p[1], 'o', ms=100 * delta2 + 4, mec='g', mfc='none')
        plt.plot(p[0], p[1], '4b', ms=100 * delta1 + 4)
        plt.xlabel("${0:.3f}$, ${1:.3f}$, ${2:.3f}$ ($\Delta^1={3:.3f}$)".\
                   format(score1[0], score1[1], score1[1], delta1) + "\n" + \
                   "${0:.3f}$, ${1:.3f}$ , ${2:.3f}$ ($\Delta^2={3:.3f}$)".\
                   format(score2[0], score2[1], score2[2], delta2))
        plt.axis('image')

        plt.subplot(122)
        plt.imshow(H, interpolation='none')
        plt.plot(tmp_thetas, tmp_rhos, 'xk')
        plt.axis('image')

        plt.savefig('output/debug/triangle1-{0}.pdf'.\
                    format(str.zfill(repr(number), 4)))
        #np.savetxt('output/debug/triangle1-{0}.dat'.\
        #            format(str.zfill(repr(number), 4)), im)

    return number, \
           score1[0], score1[1], score1[2], delta1, \
           score2[0], score2[1], score2[2], delta2, \
           p[0], p[1], sigma_p[0], sigma_p[1], pn, \
           cs[0, 0], cs[0, 1], cs[1, 0], cs[1, 1], cs[2, 0], cs[2, 1]


## Triangle 2:
def evaluate_triangle2(number):
    im, dims = get_survey(number, True)

    tl = np.round(np.array(Conf['triangle2']['topleft']) * dims).\
         astype(np.int)
    br = np.round(np.array(Conf['triangle2']['bottomright']) * dims).\
         astype(np.int)

    T = im[tl[0]: br[0], tl[1]: br[1]].copy()
    T -= T.max()
    T = np.round(np.abs(T)).astype(bool)

    ### Compuations:
    r, t, H = ht.hough_transform(T, 0.2, 1.0)
    tmp_rhos, tmp_thetas = tri.get_maxima(H, neigh=15)  # 15 is a magic number
    rhos = r[np.round(tmp_rhos).astype(np.int)]
    thetas = t[np.round(tmp_thetas).astype(np.int)]
    ks, ms = tri.get_lines_params(rhos, thetas)
    lines = np.array([ks, ms]).T
    cs = np.array(tri.get_corners(lines))
    wT, mask = tri.wash_triangle(T, cs, 8)
    score1, delta1, p, sigma_p, pn = tri.get_score(wT, cs, 1)
    score2, delta2, p, sigma_p, pn = tri.get_score(wT, cs, 2)

    if Conf['debug']:
        print "Writing debug..."

        f1 = plt.figure(1)
        f1.clf()
        plt.title('survey-{0}'.format(str.zfill(repr(number), 4)))
        plt.subplot(121)
        plt.imshow(T, interpolation='none', cmap=plt.cm.gray_r)
        plt.plot(np.hstack((cs[:, 0], cs[0, 0])), \
                 np.hstack((cs[:, 1], cs[0, 1])), 'r--1')
        plt.plot(p[0], p[1], 'o', ms=100 * delta2 + 4, mec='g', mfc='none')
        plt.plot(p[0], p[1], '4b', ms=100 * delta1 + 4)
        plt.xlabel("${0:.3f}$, ${1:.3f}$, ${2:.3f}$ ($\Delta^1={3:.3f}$)".\
                   format(score1[0], score1[1], score1[1], delta1) + "\n" + \
                   "${0:.3f}$, ${1:.3f}$ , ${2:.3f}$ ($\Delta^2={3:.3f}$)".\
                   format(score2[0], score2[1], score2[2], delta2))
        plt.axis('image')

        plt.subplot(122)
        plt.imshow(H, interpolation='none')
        plt.plot(tmp_thetas, tmp_rhos, 'xk')
        plt.axis('image')

        plt.savefig('output/debug/triangle2-{0}.pdf'.\
                    format(str.zfill(repr(number), 4)))
        #np.savetxt('output/debug/triangle2-{0}.dat'.\
        #           format(str.zfill(repr(number), 4)), im)

    return number, \
           score1[0], score1[1], score1[2], delta1, \
           score2[0], score2[1], score2[2], delta2, \
           p[0], p[1], sigma_p[0], sigma_p[1], pn, \
           cs[0, 0], cs[0, 1], cs[1, 0], cs[1, 1], cs[2, 0], cs[2, 1]
