import numpy as np
import scipy as sp
from matplotlib.nxutils import points_inside_poly
from hough import hough_transform as ht


def get_maxima(the_data, n_max=3, neigh=5):
    rhos   = np.zeros(n_max)
    thetas = np.zeros(n_max)
    data = the_data.copy()

    for n in xrange(n_max):
        r, t = np.unravel_index(data.argmax(), data.shape)
        rs = range(r - neigh, r + neigh + 1)
        ts = range(t - neigh, t + neigh + 1)
        R, T = np.meshgrid(rs, ts)

        d = data[(R, T)]
        rhos[n]   = (R * d).sum() / d.sum()
        thetas[n] = (T * d).sum() / d.sum()

        data[(R, T)] = data.min()

    return (rhos, thetas)


def get_line_params(rho, theta):
    k = -np.cos(theta) / np.sin(theta)
    m = rho / np.sin(theta)

    return k, m


def get_lines_params(rhos, thetas):
    ks = np.zeros(rhos.size)
    ms = np.zeros(rhos.size)

    for n in xrange(ks.size):
        ks[n], ms[n] = get_line_params(rhos[n], thetas[n])

    return ks, ms


def get_intersect(k, m):
    x = (m[1] - m[0]) / (k[0] - k[1])
    y = k[0] * x + m[0]
    return x, y


def get_corners(lines):
    n = len(lines)

    corners = []

    for c in range(n):
        for r in range(c + 1, n):
            ks = lines[(c, r), (0, 0)]
            ms = lines[(c, r), (1, 1)]
            corners.append(get_intersect(ks, ms))

    corners = np.array(corners)
    corners = order_corners(corners)

    return corners


def order_corners(corners):
    corner_set = set(range(3))

    tec_ind = corners[:, 1].argmin()
    soc_ind = corners[:, 0].argmin()
    cul_ind = corner_set.difference(set([tec_ind, soc_ind])).pop()

    return corners[(tec_ind, soc_ind, cul_ind), :]


def same_side(p1, p2, a, b):
    x1 = np.cross(b - a, p1 - a)
    x2 = np.cross(b - a, p2 - a)

    if x1 * x2 >= 0:
        return True
    else:
        return False


def point_in_triangle(p, corners):
    a, b, c = corners
    if same_side(p, a, b, c) and \
       same_side(p, b, a, c) and \
       same_side(p, c, a, b):
        return True
    else:
        return False


def wash_triangle(the_im, the_corners, offset=7):
    im = the_im.copy()
    corners = the_corners.copy()

    corners = round_to_centre(corners, offset)

    mask = get_mask(im, corners)

    im *= mask

    return im, mask


def get_mask(im, corners):
    R, C = np.meshgrid(np.arange(im.shape[1]), np.arange(im.shape[0]))

    R, C = R.flatten(), C.flatten()

    points = np.vstack((R, C)).T
    mask = points_inside_poly(points, corners)
    mask = mask.reshape(im.shape)

    return mask


def round_to_centre(corners, offset=1):
    new_cs = corners.copy()

    centre = np.array([new_cs[:, 0].mean(), new_cs[:, 1].mean()])
    for c in xrange(new_cs.shape[0]):
        vect = (new_cs[c] - centre)
        vect /= np.sqrt((vect ** 2).sum())
        new_cs[c] -= offset * vect

    return new_cs


def get_blob_centre(im):
    inds = np.array(np.where(im))

    return inds.mean(1), inds.var(1), inds.shape[1]


def get_score(im, corners, dim=2):
    p, var_p, n = get_blob_centre(im)
    p[:] = p[-1::-1]
    var_p[:] = var_p[-1::-1]
    sigma_p = np.sqrt(var_p)

    if dim == 2:
        norm = np.cross(corners[2] - corners[0], corners[1] - corners[0])

        cul = np.cross(p - corners[0], corners[1] - corners[0]) / norm
        tec = np.cross(p - corners[1], corners[2] - corners[1]) / norm
        soc = np.cross(p - corners[2], corners[0] - corners[2]) / norm

        delta = (sigma_p[0] ** 2 + sigma_p[1] ** 2 - \
                 sigma_p[0] * sigma_p[1]) / norm
    elif dim == 1:
        centre = np.array([corners[:, 0].mean(), corners[:, 1].mean()])

        dtec = np.sqrt(((corners[0] - p) ** 2).sum())
        dsoc = np.sqrt(((corners[1] - p) ** 2).sum())
        dcul = np.sqrt(((corners[2] - p) ** 2).sum())
        norm = 2 * (dtec + dsoc + dcul)

        tec = (dsoc + dcul) / norm
        soc = (dtec + dcul) / norm
        cul = (dsoc + dtec) / norm

        delta = np.sqrt(var_p.sum()) / norm
    else:
        print "Invalid dimension: {0}! Using 2.".format(dim)
        return get_score(im, corners)

    score = (tec, soc, cul)

    print score

    return score, delta, p, sigma_p, n
