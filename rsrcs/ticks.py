import numpy as np


def get_pinning(the_data, h=2):
    data = the_data.copy()

    C = np.where(data.mean(0) > data.mean(), 1, 0)
    R = np.where(data.mean(1) > data.mean(), 1, 0)

    h = 2
    d = np.hstack((np.ones(h), -np.ones(h)))

    dC = np.convolve(C, d, 'same')
    np.where(np.abs(dC) == h, 1, 0)

    dR = np.convolve(R, d, 'same')
    np.where(np.abs(dR) == h, 1, 0)
