import numpy as np


def hough_transform(image, theta_res=1.0, rho_res=1.0):
    rows, cols = image.shape

    theta = np.linspace(0, 180.0, np.ceil(180.0 / theta_res) + 1) * np.pi / 180
    thetax = theta.reshape((1, theta.size))

    D = np.sqrt((rows - 1) ** 2 + (cols - 1) ** 2)
    q = np.ceil(D / rho_res)
    nrho = 2 * q + 1
    rho = np.linspace(-q * rho_res, q * rho_res, nrho)

    H = np.zeros((len(rho), len(theta)))

    data = np.where(image)
    rhos = data[0].reshape((data[0].size, 1)) * np.sin(thetax) + \
           data[1].reshape((data[1].size, 1)) * np.cos(thetax)
    rind = np.round(q * rhos / D + q).astype(np.int)
    tind = np.arange(theta.size)

    for row in rind[:]:
        H[(row, tind)] += 1

    ## This is unfortunately slower:
    #tind = np.arange(theta.size).reshape((1, theta.size)) * \
    #       np.ones((data[0].size, 1)).astype(np.int)
    #
    #hist, e1, e1 = np.histogram2d(rhos.reshape(rhos.size),
    #                              tind.reshape(tind.size), H.shape)
    #H += hist

    return rho, theta, H
